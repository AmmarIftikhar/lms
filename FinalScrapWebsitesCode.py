from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd #install chrom webdriver
import openpyxl
import csv

excel=openpyxl.Workbook()
sheet=excel.active
sheet.append(['TITLE','YEAR','SIZE','PAGES','DOWNLOADS','LANGUAGE','DESCRIPTION'])

Pages=0
Size=0
Year=0
Download=0
BookName=""
Language=""
Description=''
links=[]
links2=[]
driver = webdriver.Chrome(executable_path='C:\Program Files (x86)\chromedriver.exe')
for i in range(1,2):
    i=str(i)
    for j in range(98,100):
        a=chr(j)
        url="https://www.pdfdrive.com/search?q="+a+"&pagecount=&pubyear=&searchin=&em=&page"+i
        links.append(str(url))
        
for i in range(1,2):
    i=str(i)
    url="https://www.readings.com.pk/pages/category.aspx?Category=&Level=&Sortby=ArrivalDate&BookType=&Page="+i
    links2.append(url)  


for z in range(len(links)):
    PagesA=[]
    BookNameA=[]
    SizeA=[]
    YearA=[]
    DownloadA=[]
    DescriptionA=[]
    LanguageA=[]
    driver.get(links[z])
    content = driver.page_source
    soup = BeautifulSoup(content,"html.parser" )
    var1=soup.findAll('div',attrs={'class':'file-right'})
    for a in var1 :
        data=a.find('span',attrs={'class':'fi-pagecount'})
        if data == None:
            Pages=0
            PagesA.append(0)
        else:
            Pages=data.text
            PagesA.append(data.text)

        data=a.find('span',attrs={'class':'fi-year'})
        if data == None:
             Year=0
             YearA.append(0)

        else:
             Year=data.text
             YearA.append(data.text)

        data=a.find('span',attrs={'class':'fi-size hidemobile'})
        if data == None:
             Size=0
             SizeA.append(0)

        else:
             Size=data.text
             SizeA.append(data.text)

        data=a.find('span',attrs={'class':'fi-hit'})
        if data == None:
             Download=0
             DownloadA.append(0)

        else:
             Download=data.text
             DownloadA.append(data.text)

        data=a.find('span',attrs={'class':'fi-lang'})
        if data == None:
             Language="ENGLISH"
             LanguageA.append("ENGLISH")

        else:
             Language=data.text
             LanguageA.append(data.text)
        List = [Year,Pages,Size,Download,Language]
 
        # Open our existing CSV file in append mode
        # Create a file object for this file
        data=a.find('h2')
        if data == None:
            
             BookName=0
             Description="------------"
             BookNameA.append(0)
             DescriptionA.append("-------")

        else:
            des=''
            aa=''
            ab=''
            a=data.text
            Break=0
            for i in range(len(str(a))):
                if(a[i]==':'):
                    Break=i
            if(Break !=0):
                for i in range(0,Break):
                    aa=aa+a[i]   
                BookName=aa
                BookNameA.append(aa)
                        
                for i in range(Break+1,len(str(a))):
                    ab=ab+a[i]   
                Description=ab
                DescriptionA.append(ab)
                    
            else:
                BookName=a
                BookNameA.append(a)
                Description="NO DESCRIPTION"
                DescriptionA.append("NO DESCRIPTION")
        List=[BookName,Year,Size,Pages,Download,Language,Description]
        with open('FORTEST.csv', 'a',encoding="utf-8") as f_object:
            writer_object = csv.writer(f_object,lineterminator = '\n')
            writer_object.writerow(List)
            f_object.close()
        
        
   
     