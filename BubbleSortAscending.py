# -*- coding: utf-8 -*-
"""
Created on Mon Oct 17 22:57:24 2022

@author: Administrator
"""

def BubbleSort(Array):
    size=len(Array)
    temp=0
    
    for i in range(0,size-1):
        for j in range(size-1):
            if Array[j]>Array[j+1]:
                temp=Array[j]
                Array[j]=Array[j+1]
                Array[j+1]=temp
    print(Array) 
    


def main():
    Array=[6,5,7,8]
    BubbleSort(Array)
    

if __name__=="__main__":
    main()