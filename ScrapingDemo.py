from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd #install chrom webdriver
BookName=[]
Pages=[]
data=""
Year=[]
Size=[]
Description=[]
Download=[]
Url=[]
driver = webdriver.Chrome(executable_path='C:\Program Files (x86)\chromedriver.exe')
driver.get("https://www.pdfdrive.com/")
content = driver.page_source
soup = BeautifulSoup(content,"html.parser" )
for a in soup.findAll('div',attrs={'class':'file-info'}):
    data=a.find('span',attrs={'class':'fi-pagecount'})
    if data == None:
        Pages.append(0)
    else:
        Pages.append(data.text)
    data=a.find('span',attrs={'class':'fi-year'})
    if data == None:
         Year.append(0)
    else:
         Year.append(data.text)
    data=a.find('span',attrs={'class':'fi-size hidemobile'})
    if data == None:
         Size.append(0)
         Url.append(0)
    else:
         Size.append(data.text)
         Url.append(data.text[0])
    data=a.find('span',attrs={'class':'fi-hit'})
    if data == None:
         Download.append(0)
    else:
         Download.append(data.text)

Scraped_Data = {'TITLE':BookName,'PAGE':Pages,'YEAR':Year,'Size':Size}
Scraped_Books = pd.DataFrame(Scraped_Data)
Scraped_Books[:5]
Scraped_Books.to_csv('Scrap.csv',index = None)
    
