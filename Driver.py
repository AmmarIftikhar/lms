import sys
from PyQt5.uic import loadUi
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtWidgets import QDialog, QApplication, QWidget
import csv
from PyQt5.QtGui import QPixmap
from PyQt5 import QtCore, QtGui, QtWidgets
import pandas as pd

class WelcomeScreen(QDialog):
    def __init__(self):
        super(WelcomeScreen, self).__init__()
        loadUi("Welcome.ui",self)
        self.btnSignIn.clicked.connect(self.gotologin)
        self.btnCreate.clicked.connect(self.gotocreate)

    def gotologin(self):
        login = LoginScreen()
        widget.addWidget(login)
        widget.setCurrentIndex(widget.currentIndex()+1)

    def gotocreate(self):
        create = CreateAccScreen()
        widget.addWidget(create)
        widget.setCurrentIndex(widget.currentIndex() + 1)

class LoginScreen(QDialog):
    def __init__(self):
        super(LoginScreen, self).__init__()
        loadUi("SignIn.ui",self)
        self.txtPass.setEchoMode(QtWidgets.QLineEdit.Password)
        self.btnSignIn.clicked.connect(self.loginfunction)
        self.btnBack.clicked.connect(self.Back)

    def loginfunction(self):
        name=self.txtUserName.text()
        password=self.txtPass.text()
        B=self.CheckPass(self.txtUserName.text(),self.txtPass.text())
        if(B==False):
            create = MainScreen()
            widget.addWidget(create)
            widget.setCurrentIndex(widget.currentIndex() + 1)
           
        else:
            msg=QMessageBox()
            msg.setWindowTitle("--- ACCOUNT LOGIN  ---")
            msg.setText("CREDENTIALS NOT VALID!")
            msg.setIcon(QMessageBox.Information)
            msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
            font=QtGui.QFont()
            font.setPointSize(12)
            font.setWeight(75)
            msg.setFont(font)
            msg.exec()
        self.txtUserName.setText("")
        self.txtPass.setText("")
    def Back(self):
        create=WelcomeScreen()
        widget.addWidget(create)
        widget.setCurrentIndex(widget.currentIndex() + 1)
    def CheckPass(self,name,password): 
        Break=0
        USERS=[]
        UserName=[]
        Passw=[]
        with open("LoginPass.txt") as f:
            lines = f.readlines()
            for i in range(len(lines)):
                user = lines[i].strip()
                USERS.append(user)
        for i in range(len(USERS)):
            Name,Pass=self.BreakThem(USERS[i])
            UserName.append(Name)
            Passw.append(Pass)
       
        for i in range(len(UserName)):
            if(str(name)==str(UserName[i]) and str(password)==str(Passw[i])):
                return False
                
        return True
        
    def BreakThem(self,A):
        aa=''
        ab=''
        for i in range(len(str(A))):
            if(A[i]==','):
                Break=i
        for i in range(0,Break):
            aa=aa+A[i]   
        userName=aa
        for i in range(Break+1,len(str(A))):
            ab=ab+A[i]   
        Pass=ab
        return userName,Pass


class CreateAccScreen(QDialog):
    def __init__(self):
        super(CreateAccScreen, self).__init__()
        loadUi("CreateAccount.ui",self)
        self.txtPass.setEchoMode(QtWidgets.QLineEdit.Password)
        self.btnSignIn.clicked.connect(self.Register)
        self.btnBack.clicked.connect(self.Back)

    def Back(self):
        create=WelcomeScreen()
        widget.addWidget(create)
        widget.setCurrentIndex(widget.currentIndex() + 1)
    def Register(self):
        name=self.txtUserName.text()
        passw=self.txtPass.text()
        Break=0
        USERS=[]
        UserName=[]
        Passw=[]
        with open("LoginPass.txt") as f:
            lines = f.readlines()
            for i in range(len(lines)):
                user = lines[i].strip()
                USERS.append(user)
        for i in range(len(USERS)):
            Name,Pass=self.BreakThem(USERS[i])
            UserName.append(Name)
            Passw.append(Pass)
        for i in range(len(UserName)):
            if(str(name)==str(UserName[i]) and str(passw)==str(Passw[i])):
                msg=QMessageBox()
                msg.setWindowTitle("--- ACCOUNT LOGIN  ---")
                msg.setText("SAME ACCOUNT ALREADY ADDED !")
                msg.setIcon(QMessageBox.Information)
                msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
                font=QtGui.QFont()
                font.setPointSize(12)
                font.setWeight(75)
                msg.setFont(font)
                msg.exec()
            else:
                with open('LoginPass.txt', 'a') as f:
                    f.write('\n')
                    f.write(name+','+passw)
                    msg=QMessageBox()
                    msg.setWindowTitle("--- ACCOUNT LOGIN  ---")
                    msg.setText("ADDED !")
                    msg.setIcon(QMessageBox.Information)
                    msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
                    font=QtGui.QFont()
                    font.setPointSize(12)
                    font.setWeight(75)
                    msg.setFont(font)
                    msg.exec()
                
        
        create=WelcomeScreen()
        widget.addWidget(create)
        widget.setCurrentIndex(widget.currentIndex() + 1)
    def BreakThem(self,A):
        aa=''
        ab=''
        for i in range(len(str(A))):
            if(A[i]==','):
                Break=i
        for i in range(0,Break):
            aa=aa+A[i]   
        userName=aa
        for i in range(Break+1,len(str(A))):
            ab=ab+A[i]   
        Pass=ab
        return userName,Pass
    
class MainScreen(QDialog):
    def __init__(self):
        super(MainScreen, self).__init__()
        loadUi("MainScreen.ui",self)
        self.btnAdd.clicked.connect(self.AddBook)
        self.btnEdit.clicked.connect(self.EditBook)
    def AddBook(self):
        create=AddBook()
        widget.addWidget(create)
        widget.setCurrentIndex(widget.currentIndex() + 1)
    def EditBook(self):
      create=EditBook()
      widget.addWidget(create)
      widget.setCurrentIndex(widget.currentIndex() + 1)
        

class AddBook(QDialog):
    def __init__(self):
        super(AddBook, self).__init__()
        loadUi("AddBook.ui",self)
        self.btnBack.clicked.connect(self.Back)
    def Back(self):
        create=MainScreen()
        widget.addWidget(create)
        widget.setCurrentIndex(widget.currentIndex() + 1)

class EditBook(QDialog):
    def __init__(self):
        super(EditBook, self).__init__()
        loadUi("EditBook.ui",self)
        self.btnBack.clicked.connect(self.Back)
    def Back(self):
        create=MainScreen()
        widget.addWidget(create)
        widget.setCurrentIndex(widget.currentIndex() + 1)

# main
app = QApplication(sys.argv)
welcome = WelcomeScreen()
widget = QtWidgets.QStackedWidget()
widget.addWidget(welcome)
widget.setFixedHeight(550)
widget.setFixedWidth(780)
widget.show()
sys.exit(app.exec_())
