def BinarySearch(Array,key,low,high):   #low=0 and high = len(Array)-1 key is element to be found
    while high>=low:
        mid = low + (high-low)//2
        if Array[mid]==key:
            return mid
        elif Array[mid]<key:
            low=mid+1
        else:
            high=mid-1
    return -1
